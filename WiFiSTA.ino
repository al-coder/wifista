/*
 *  Template for Network applications
 */
#pragma once

#define SERIAL_DEBUG_BAUD 115200  // Debug BAUD should be same in the IDE Tools.Serial_Monitor
//#include "esp_log.h"

#include "xService.h"

//#include "WiFiClientSecure.h"  //"WiFi.h"
#include <WiFiMulti.h>
WiFiMulti wifiMulti;
#include "esp_wpa2.h"

//#define EAP_ANONYMOUS_IDENTITY "anonymous@example.com" //anonymous identity
//#define EAP_IDENTITY "id@example.com"                  //user identity
//#define EAP_PASSWORD "41813065" //eduroam user password

//#define GPIO_ANT1   2   // GPIO for antenna 1
//#define GPIO_ANT2   25  // GPIO for antenna 2 (default)

#include "myWiFiConfig.h" // My WiFi configuration (tag: credentials). Keeps only: const char* ssid = "******"; const char* password = "******"; //excluded in .gitignore
const int minPowerReq = -36; //(dB) lower ESP32 w/o external anthena w/o chances

#include "httpsService.h" // My (copy/pasted service unit)
#include "UdpNtpClient.h" // My (copy/pasted service unit)

const unsigned int deepFreezeTime = 1000*5; //1000*60*3;

uint8_t wifiStatus;

bool establishWiFiUnderProccessing = false;
bool syncTimeProviderIsSet = false;

//------------------------------------------------------

void halt() {
  digitalWrite(LED_BUILTIN, HIGH);
  for (;;) {}
}

void deepFreeze(int t) { // Real Deep Sleep wasn't realized !!!
  delay(t);
}
  
void blink_buildin(int times = 0, int blink_duration = 200) {
  int i;
  for (i = 0; i<=times; i++) {
    digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
    delay(blink_duration);                       // wait for a second
    digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
    delay(blink_duration);                       // wait for a second
  }
}

/*
String translateEncryptionType(wifi_auth_mode_t encryptionType) {
  switch (encryptionType) {
    case (WIFI_AUTH_OPEN):
      return "Open";
    case (WIFI_AUTH_WEP):
      return "WEP";
    case (WIFI_AUTH_WPA_PSK):
      return "WPA_PSK";
    case (WIFI_AUTH_WPA2_PSK):
      return "WPA2_PSK";
    case (WIFI_AUTH_WPA_WPA2_PSK):
      return "WPA_WPA2_PSK";
    case (WIFI_AUTH_WPA2_ENTERPRISE):
      return "WPA2_ENTERPRISE";
  }
}

void scan() {
    Serial.println(tagDebug + "start: scan");
    int n = WiFi.scanNetworks(); // wifiMulti.scanNetworks will return the number of networks found
    Serial.println(tagDebug + "done: scan");
    if (n == 0) {
        Serial.println(tagInfo + "no networks found");
    } else {
        Serial.println(tagInfo + String(n) + " networks found:");
        for (int i = 0; i < n; ++i) {
            // Print SSID and RSSI for each network found
            Serial.print(tagInfo + String(i) + ": " + WiFi.SSID(i) + " (" + WiFi.RSSI(i) + "dB)");
            //Serial.println((wifiMulti.encryptionType(i) == WIFI_AUTH_OPEN)?" ":"*");
            String encryptionTypeDescription = translateEncryptionType(WiFi.encryptionType(i));
            Serial.println(encryptionTypeDescription);
            //also: WiFi.BSSIDstr()-(remote MAC), WiFi.gatewayIP(), WiFi.subnetMask(), WiFi.RSSI()+"dB", WiFi.localIP(), WiFi.macAddress() (esp32 MAC)
        }
    }
}
*/

int checkApOnAndStrong(String apName, int reqPower) { //check if AP ON and Strong
    int result = -1; //Error: unknown
    Serial.println(tagDebug + "start: checkAppONandStrong");  
    int n = WiFi.scanNetworks(); // wifiMulti.scanNetworks will return the number of networks found
    Serial.println(tagDebug + "done: scan");
    if (n == 0) {
        Serial.println(tagInfo + "no networks found");
        result = -2; //Error: no networks found
    } else {
        result = -3; //Error: AP was't found
        Serial.println(tagInfo + String(n) + " networks found");
        for (int i = 0; i < n; ++i) {
            if (WiFi.SSID(i) == apName) {
              Serial.println(tagInfo + "AP " + apName + " found. Power: " + WiFi.RSSI(i) + "dB");
              (WiFi.RSSI(i) >= reqPower) ? result = 0 : result = -4;
            }
        }
    }
    return result;
}

void establishCastomWiFi() {
    Serial.println(tagDebug + "Wifi STA connecting to " + ssid + "...");
    
    //esp_wifi_sta_wpa2_ent_set_identity((uint8_t *)EAP_ANONYMOUS_IDENTITY, strlen(EAP_ANONYMOUS_IDENTITY)); //provide identity
    //esp_wifi_sta_wpa2_ent_set_username((uint8_t *)EAP_IDENTITY, strlen(EAP_IDENTITY)); //provide username
    //esp_wifi_sta_wpa2_ent_set_password((uint8_t *)EAP_PASSWORD, strlen(EAP_PASSWORD)); //provide password
    //esp_wifi_sta_wpa2_ent_enable();

    WiFi.begin(ssid, password); //wifiMulti.APlist.clear();-private, doesn't work, so // wifiMulti.addAP(ssid, password);
    Serial.println(tagDebug + "Waiting for WiFi...");

    uint32_t attempts = 0;
    for(;;) {
        wifiStatus = WiFi.waitForConnectResult(); //WiFi.status(); / WiFi.run();
        if (wifiStatus == WL_CONNECTED) {       
            Serial.println(tagDebug + "connected (WL_CONNECTED)");
            break;
        } else {                
            attempts++;
            if (attempts > 16) { // Reset board if not connected after 16th attamts
                Serial.println("Resetting due to Wifi not connecting...");
                ESP.restart();
            }
            Serial.println(tagDebug + "WiFi STA status: " + get_wifi_status(wifiStatus));
            delay(100);
        }  
    }

    Serial.println(tagDebug + tagInfo + "WiFi established. IP address: " + WiFi.localIP());
}

void establishWiFi() {
  if (!establishWiFiUnderProccessing) {
    establishWiFiUnderProccessing = true;
    int err = checkApOnAndStrong(ssid, minPowerReq);
    switch(err) {
      case  0: 
        establishCastomWiFi();
        break;
      case -1: 
        Serial.println(tagError + "Error: checkApOnAndStrong: unknow");
        ESP.restart();
        break;
      case -2: 
        Serial.println(tagError + "Error: checkApOnAndStrong: no networks found");
        deepFreeze(deepFreezeTime);
        break;
      case -3: 
        Serial.println(tagError + "Error: checkApOnAndStrong: AP '" + ssid + "' was't found");
        deepFreeze(deepFreezeTime);
        break;
      case -4: 
        Serial.println(tagError + "Error: checkApOnAndStrong: AP's signal too low");
        delay(5000);
        break;
    }
    establishWiFiUnderProccessing = false;
  }  
}

void onWiFiEvent(WiFiEvent_t event, WiFiEventInfo_t info) {
  Serial.println(tagDebug + tagWiFiEvent + " event: " + event);

  switch (event) {
    case SYSTEM_EVENT_WIFI_READY: 
      Serial.println(tagDebug + tagWiFiEvent + "WiFi interface ready");
      break;
    case SYSTEM_EVENT_SCAN_DONE:
      Serial.println(tagDebug + tagWiFiEvent + "Completed scan for access points");
      break;
    case SYSTEM_EVENT_STA_START:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi client started");
      break;
    case SYSTEM_EVENT_STA_STOP:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi clients stopped");
      break;
    case SYSTEM_EVENT_STA_CONNECTED:
      Serial.println(tagDebug + tagWiFiEvent + "Connected to access point");
      break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
      Serial.println(tagDebug + tagWiFiEvent + "Disconnected from WiFi access point");
      WiFi.begin(ssid, password);
      break;
    case SYSTEM_EVENT_STA_AUTHMODE_CHANGE:
      Serial.println(tagDebug + tagWiFiEvent + "Authentication mode of access point has changed");
      break;
    case SYSTEM_EVENT_STA_GOT_IP:
      Serial.println(tagDebug + tagWiFiEvent + "Obtained IP address: " + WiFi.localIP()); // or IPAddress(info.got_ip.ip_info.ip.addr)
      break;
    case SYSTEM_EVENT_STA_LOST_IP:
      Serial.println(tagDebug + tagWiFiEvent + "Lost IP address and IP address is reset to 0");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_SUCCESS:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi Protected Setup (WPS): succeeded in enrollee mode");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_FAILED:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi Protected Setup (WPS): failed in enrollee mode");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_TIMEOUT:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi Protected Setup (WPS): timeout in enrollee mode");
      break;
    case SYSTEM_EVENT_STA_WPS_ER_PIN:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi Protected Setup (WPS): pin code in enrollee mode");
      break;
    case SYSTEM_EVENT_AP_START:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi access point started");
      break;
    case SYSTEM_EVENT_AP_STOP:
      Serial.println(tagDebug + tagWiFiEvent + "WiFi access point  stopped");
      break;
    case SYSTEM_EVENT_AP_STACONNECTED:
      Serial.println(tagDebug + tagWiFiEvent + "Client connected");
      break;
    case SYSTEM_EVENT_AP_STADISCONNECTED:
      Serial.println(tagDebug + tagWiFiEvent + "Client disconnected");
      break;
    case SYSTEM_EVENT_AP_STAIPASSIGNED:
      Serial.println(tagDebug + tagWiFiEvent + "Assigned IP address to client");
      break;
    case SYSTEM_EVENT_AP_PROBEREQRECVED:
      Serial.println(tagDebug + tagWiFiEvent + "Received probe request");
      break;
    case SYSTEM_EVENT_GOT_IP6:
      Serial.println(tagDebug + tagWiFiEvent + "IPv6 is preferred");
      break;
    case SYSTEM_EVENT_ETH_START:
      Serial.println(tagDebug + tagWiFiEvent + "Ethernet started");
      break;
    case SYSTEM_EVENT_ETH_STOP:
      Serial.println(tagDebug + tagWiFiEvent + "Ethernet stopped");
      break;
    case SYSTEM_EVENT_ETH_CONNECTED:
      Serial.println(tagDebug + tagWiFiEvent + "Ethernet connected");
      break;
    case SYSTEM_EVENT_ETH_DISCONNECTED:
      Serial.println(tagDebug + tagWiFiEvent + "Ethernet disconnected");
      break;
    case SYSTEM_EVENT_ETH_GOT_IP:
      Serial.println(tagDebug + tagWiFiEvent + "Obtained IP address");
      break;
    default: break;
  }
}

void setup() {
    int err;
    
    // initialize digital pin LED_BUILTIN
    pinMode(LED_BUILTIN, OUTPUT);
    blink_buildin();

    // initialize Serial Output & digital pin LED_BUILTIN
    Serial.begin(SERIAL_DEBUG_BAUD);
    while (!Serial) {;}    
    Serial.println(tagDebug + "Application started");

    // Set WiFi to station mode and disconnect from an AP if it was previously connected
    WiFi.disconnect(true, true);
    if (WiFi.isConnected()) {
      Serial.println(tagError + "WiFi.disconnect error!");
      halt();
    }
    
    WiFi.mode(WIFI_OFF);
    WiFi.mode(WIFI_STA);
    WiFi.onEvent(onWiFiEvent);


    //этот код по событию есть инет а не коннекция иначе не сработает если сразу не законнектится  !!!!!!!!!!!!!!!!!!!
    //establishWiFi();
    Serial.println(printTime());
}

String get_wifi_status(int status) {
    switch(status){
        case WL_IDLE_STATUS:
        return "WL_IDLE_STATUS";
        case WL_SCAN_COMPLETED:
        return "WL_SCAN_COMPLETED";
        case WL_NO_SSID_AVAIL:
        return "WL_NO_SSID_AVAIL";
        case WL_CONNECT_FAILED:
        return "WL_CONNECT_FAILED";
        case WL_CONNECTION_LOST:
        return "WL_CONNECTION_LOST";
        case WL_CONNECTED:
        return "WL_CONNECTED";
        case WL_DISCONNECTED:
        return "WL_DISCONNECTED";
    }
}

void setSyncTimeProvider() {
    Serial.println(printTime());    
    //getNtpTime();
    timeZone = -5;
    setSyncProvider(getNtpTime);
    setSyncInterval(300);    
    Serial.println(printTime());
    syncTimeProviderIsSet = true;
}

void loop() {
  if (WiFi.isConnected()) {
    blink_buildin(1, 750);
    if (!syncTimeProviderIsSet) {
        setSyncTimeProvider();  //inside getNtpTime function поидее тоже флаг должен сбиваться после успешного получения времени. А сейчас если к Wifi законектился а Инета нет, он не запрашивает опять время хоть и "setSyncProvider". - А нет, сделал сам :)
        getHTTP();
    } 
    Serial.println(printTime());
  } else {
    establishWiFi();
  }

/*    
    Serial.println("");    
    delay(5000);  // Wait a bit before scanning again
    //WiFi.disconnect(true);
    //WiFi.mode(WIFI_OFF);
*/    
}

//Serial.println(WiFi.isConnected()?"Error: Still Connected":"Ok: disconnected");

/*  
    // my Droduino32 support only 2.4Gh signal
    bool err = false;
    err = WiFi.setDualAntennaConfig(GPIO_ANT1, GPIO_ANT2, WIFI_RX_ANT_AUTO, WIFI_TX_ANT_AUTO);
    if(err == false) {
        Serial.println("Dual Antenna configuration failed!");
    } else {
        Serial.println("Dual Antenna configuration successfuly done!");
    }
*/

//Serial.println("Setup done, connecting ESP32 MAC: " + WiFi.macAddress() + ".");
//yield();
