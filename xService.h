#pragma once

static const String tagError = "[E]";
static const String tagWarning = "[W]";
static const String tagDebug = "[D]";
static const String tagInfo = "[I]";
static const String tagWiFiEvent = "[WiFi]";
static const String tagNTP = "[NTP]";
